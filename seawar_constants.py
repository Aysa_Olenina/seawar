# -*- coding: utf-8 -*-

CONNECTION_FAILED = 0
CONNECTION_SUCCESS = 1
CONNECTION_DISCONNECT = 2

GAME_WAIT = 11
GAME_START = 12
GAME_FINISH = 13

GAME_WIN = 14
GAME_DEFEAT = 15
GAME_TECHNICAL_WIN = 16
GAME_TECHNICAL_DEFEAT = 17

STATUS_WAIT_ENEMY = 31
STATUS_YOUR_PLAY = 32
STATUS_BAD_MAP = 33
STATUS_OK = 34

STATUS_WRONG_COORD = 35
STATUS_SHOOT_DOWN = 36
STATUS_MISS = 37
STATUS_GOOD_SHOT = 38
STATUS_BAD_SHOOT = 39

STATUSES = {
    CONNECTION_FAILED: u'Вам не удалось подключиться к серверу',
    CONNECTION_SUCCESS: u'Вы подключились к серверу',
    CONNECTION_DISCONNECT: u'Соединение потеряно, попробуйте заново',

    GAME_WAIT: u'Ожидание соперника...',
    GAME_START: u'Да начнётся битва!!!',
    GAME_FINISH: u'Игра окончена.',

    GAME_WIN: u'Поздравляю! Вы выиграли!',
    GAME_DEFEAT: u'Вы проиграли! Вы неудачник!',
    GAME_TECHNICAL_WIN: u'Вы выиграли. (Техническое поражение соперника)',
    GAME_TECHNICAL_DEFEAT: u'Sorry! Техническое поражение',

    STATUS_WAIT_ENEMY: u'Ожидание хода противника...',
    STATUS_YOUR_PLAY: u'Ваш ход. Куда стрелять? (например: a1)',
    STATUS_WRONG_COORD: u'Неверные координаты, введите заново',

    STATUS_BAD_MAP: u'Неверная карта',
    STATUS_OK: u'Ок',

    STATUS_SHOOT_DOWN: u'По Вашему кораблю попали',
    STATUS_MISS: u'Соперник промахнулся',
    STATUS_GOOD_SHOT: u'Попал!',
    STATUS_BAD_SHOOT: u'Промах :(',
}
