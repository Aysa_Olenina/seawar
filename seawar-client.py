# -*- coding: utf-8 -*-
import pickle
import sys
import time

from socket import *

import seawar_settings

from seawar_constants import *
from seawar_utils import *


class SeaWarClient(object, UtilsMixin):

    HOST = 'localhost'
    PORT = seawar_settings.DEFAULT_PORT

    def __init__(self, host=None, port=None, *args, **kwargs):

        host = host or self.HOST
        port = port or self.PORT

        if len(sys.argv) > 1:
            host = sys.argv[1]
            if len(sys.argv) > 2:
                port = int(sys.argv[2])

        self.connection = socket(AF_INET, SOCK_STREAM)
        try:
            self.connection.connect((host, port))
        except Exception:
            print_game_status(CONNECTION_FAILED)
            self.connection.close()
            return

        print_game_status(CONNECTION_SUCCESS)

        try:
            game_status = self.recv()
        except DisconnectError:
            print_game_status(CONNECTION_DISCONNECT)
            game_status = None

        if game_status == GAME_WAIT:
            print_game_status(game_status)
            try:
                game_status = self.recv()
            except DisconnectError:
                print_game_status(CONNECTION_DISCONNECT)
                game_status = None

        if game_status == GAME_START:
            print_game_status(game_status)
            try:
                self.start_game()
            except DisconnectError:
                print_game_status(CONNECTION_DISCONNECT)
                game_status = None

        print_game_status(GAME_FINISH)
        self.connection.close()

    def start_game(self):
        self.my_battle_map = self.load_battle_map()
        self.enemy_battle_map = self.create_battle_map()

        battle_map_status = self.send_recv(self.my_battle_map)
        if battle_map_status in [STATUS_BAD_MAP, GAME_TECHNICAL_WIN]:
            print_game_status(battle_map_status)
            return

        self.print_battle_map()

        coords = None
        while True:
            action = self.recv()

            if action in [GAME_WIN, GAME_DEFEAT, GAME_TECHNICAL_WIN, GAME_TECHNICAL_DEFEAT]:
                print_game_status(action)
                return
            elif action in [STATUS_WAIT_ENEMY]:
                print_game_status(action)
            elif action in [STATUS_YOUR_PLAY, STATUS_WRONG_COORD]:
                print_game_status(action)
                coords = None
                while coords is None:
                    coords = raw_input('>>')
                    coords = self.normalize_coords(coords)
                    if coords is None:
                        print_game_status(STATUS_WRONG_COORD)

                self.send(coords)

            elif action in [STATUS_SHOOT_DOWN, STATUS_MISS]:
                normal_coords = self.recv()
                self.get_shoot_status(normal_coords, self.my_battle_map)
                self.print_battle_map()
                print_game_status(action)

            elif action in [STATUS_GOOD_SHOT, STATUS_BAD_SHOOT]:
                if action == STATUS_GOOD_SHOT:
                    self.get_shoot_status(coords, self.enemy_battle_map, field_status=2)
                elif action == STATUS_BAD_SHOOT:
                    self.get_shoot_status(coords, self.enemy_battle_map, field_status=3)
                self.print_battle_map()
                print_game_status(action)

    @property
    def now(self):
        return time.ctime(time.time())

    def send(self, data):
        try:
            data = pickle.dumps(data)
            self.connection.send(data)
        except:
            raise DisconnectError()
        return True

    def recv(self):
        try:
            data = self.connection.recv(1024)
            data = pickle.loads(data)
        except:
            raise DisconnectError()
        return data

    def send_recv(self, data):
        self.send(data)
        data = self.recv()
        return data

    def create_battle_map(self):
        battle_map = []
        for i in xrange(10):
            battle_map.append([0 for j in xrange(10)])
        return battle_map

    def load_battle_map(self):
        battle_map = self.create_battle_map()
        with open('battle_map.txt', 'r') as f:
            for row in f:
                row_number = row[:2].strip()
                if not row_number.isdigit():
                    continue
                y_coord = int(row_number) - 1
                row = row[4:14]
                for x_coord, value in enumerate(row):
                    if value == '#':
                        battle_map[y_coord][x_coord] = 1
        return self.validate_battle_map(battle_map)

    def print_battle_map(self):
        print
        print '-' * 34
        print '  ||abcdefghik||    ||abcdefghik||'
        print '==||==========||  ==||==========||'
        for y_coord in xrange(10):
            if y_coord < 9:
                y_coord_str = ' %d' % (y_coord + 1)
            else:
                y_coord_str = str(y_coord + 1)

            print '%s||%s||  %s||%s||' % (
                y_coord_str,
                self.generate_row_str(self.my_battle_map[y_coord]),
                y_coord_str,
                self.generate_row_str(self.enemy_battle_map[y_coord])
            )
        print '==||==========||  ==||==========||\n'

    def generate_row_str(self, row):
        row_str = ''
        for x_coord in xrange(10):
            battle_map_value = row[x_coord]
            if battle_map_value == 0:
                row_str += '~'
            elif battle_map_value == 1:
                row_str += '#'
            elif battle_map_value == 2:
                row_str += '@'
            elif battle_map_value == 3:
                row_str += '.'
            else:
                row_str += '~'
        return row_str


if __name__ == '__main__':
    SeaWarClient()
