# -*- coding: utf-8 -*-

from seawar_constants import STATUSES


class DisconnectError(Exception):
    pass


def print_game_status(status):
    print STATUSES[status]


class UtilsMixin:

    AVAILABLE_COORD_VALUES = range(10)
    AVAILABLE_COORD_LETTER_VALUES = 'abcdefghik'

    def validate_battle_map(self, battle_map):
        return battle_map

    def validate_normal_coords(self, normal_coords):
        if normal_coords and\
                hasattr(normal_coords, '__iter__') and\
                len(normal_coords) == 2 and\
                normal_coords[0] in self.AVAILABLE_COORD_VALUES and\
                normal_coords[1] in self.AVAILABLE_COORD_VALUES:
            return True
        return False

    def normalize_coords(self, coords):
        if not coords:
            return None
        coords = coords.strip().replace('\n', '')[:3].lower()

        x_coord = self.AVAILABLE_COORD_LETTER_VALUES.find(coords[0])
        y_coord = coords[1:].strip()

        if y_coord.isdigit():
            y_coord = int(y_coord) - 1

        normal_coords = (y_coord, x_coord)
        if self.validate_normal_coords(normal_coords):
            return normal_coords
        return None

    def get_shoot_status(self, coords, battle_map, field_status=None):
        y_coord, x_coord = coords
        if battle_map[y_coord][x_coord] == 1:
            battle_map[y_coord][x_coord] = field_status or 2
            return True
        elif battle_map[y_coord][x_coord] == 0:
            battle_map[y_coord][x_coord] = field_status or 3
        return False
