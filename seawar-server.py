# -*- coding: utf-8 -*-
import pickle
import sys
import time
import thread

from socket import *

import seawar_settings

from seawar_constants import *
from seawar_utils import *


class SeaWarServer(object, UtilsMixin):

    HOST = ''
    PORT = seawar_settings.DEFAULT_PORT

    MAX_CLIENTS_NUM = 4

    def __init__(self, port=None, max_clients_num=None, *args, **kwargs):
        port = port or self.PORT
        max_clients_num = max_clients_num or self.MAX_CLIENTS_NUM

        if len(sys.argv) > 1:
            port = int(sys.argv[1])
            if len(sys.argv) > 2:
                max_clients_num = int(sys.argv[2])

        self.socket_obj = socket(AF_INET, SOCK_STREAM)
        self.socket_obj.bind((self.HOST, port))
        self.socket_obj.listen(max_clients_num)

        self.connection_queue = []

    @property
    def now(self):
        return time.ctime(time.time())

    def run(self):
        print 'Server start'
        print 'Wait clients...'
        while True:
            connection, address = self.socket_obj.accept()
            print 'Server connected by address %s at %s' % (address, self.now)

            self.connection_queue.append(connection)

            if len(self.connection_queue) < 2:
                try:
                    self.send(connection, GAME_WAIT)
                except DisconnectError:
                    if self.connection_queue:
                        connection = self.connection_queue.pop()
                        connection.close()
                continue

            self.start_game()

    def start_game(self):
        client1_connection, client2_connection = self.connection_queue
        self.connection_queue = []

        thread.start_new_thread(self.clients_handler, (client1_connection, client2_connection))

    def clients_handler(self, client1_connection, client2_connection):
        try:
            self.send(client1_connection, GAME_START)
            self.send(client2_connection, GAME_START)

            map1 = self.recv(client1_connection)
            map2 = self.recv(client2_connection)

            if self.validate_battle_map(map1) is None:
                self.send(client1_connection, STATUS_BAD_MAP)
                self.send(client2_connection, GAME_TECHNICAL_WIN)

            elif self.validate_battle_map(map2) is None:
                self.send(client2_connection, STATUS_BAD_MAP)
                self.send(client1_connection, GAME_TECHNICAL_WIN)

            else:
                self.send(client1_connection, STATUS_OK)
                self.send(client2_connection, STATUS_OK)

            win = 0
            state = 1
            players_data = {
                0: {
                    'connection': client1_connection,
                    'map': map1
                },
                1: {
                    'connection': client2_connection,
                    'map': map2
                }
            }
            while win == 0:
                self.send(players_data[state]['connection'], STATUS_YOUR_PLAY)
                self.send(players_data[state ^ 1]['connection'], STATUS_WAIT_ENEMY)

                right_coord = False
                while not right_coord:
                    normal_coord_shoot = self.recv(players_data[state]['connection'])
                    right_coord = self.validate_normal_coords(normal_coord_shoot)
                    if not right_coord:
                        self.send(players_data[state]['connection'], STATUS_WRONG_COORD)
                        time.sleep(1)

                if self.get_shoot_status(normal_coord_shoot, players_data[state ^ 1]['map']):
                    self.send(players_data[state]['connection'], STATUS_GOOD_SHOT)
                    time.sleep(1)
                    self.send(players_data[state ^ 1]['connection'], STATUS_SHOOT_DOWN)
                    time.sleep(1)
                    self.send(players_data[state ^ 1]['connection'], normal_coord_shoot)
                    time.sleep(1)

                else:
                    self.send(players_data[state]['connection'], STATUS_BAD_SHOOT)
                    time.sleep(1)
                    self.send(players_data[state ^ 1]['connection'], STATUS_MISS)
                    time.sleep(1)
                    self.send(players_data[state ^ 1]['connection'], normal_coord_shoot)
                    state ^= 1

        except DisconnectError:
            for connection in [client1_connection, client2_connection]:
                try:
                    self.send(connection, GAME_TECHNICAL_WIN)
                except:
                    pass
                finally:
                    connection.close()

    @staticmethod
    def send(connection, data):
        try:
            data = pickle.dumps(data)
            connection.send(data)
        except:
            raise DisconnectError()
        return True

    @staticmethod
    def recv(connection):
        try:
            data = connection.recv(1024)
            data = pickle.loads(data)
        except:
            raise DisconnectError()
        return data


if __name__ == '__main__':
    server = SeaWarServer()
    server.run()
